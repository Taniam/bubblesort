package arreglos;

/**
 *
 * @author Ulises
 */
public class Aleatorio {
    
    /**
     * Genrea un valor aleatorio entre 0 y 100
     * @return Un entero con valor aleatorio entre 0 y 100
     */
    public static int ceroACien(){
        return (int) (Math.random() * 100);
    }//Calcula un valor aleatorio entre 0 y 100
    
    
    /**
     * Calcula un valor aleatorio con distribucion uniforme entre n y m enteros
     * @param n valor donde empieza el intervao
     * @param m valor donde termina el intervalo
     * @return un valor aleatorio entre x tq x esta en [n,m]
     */
    public static int n_A_M(int n, int m){
        if (n >= m || n < 0){
            return -1;  // Error
        }else if( n == 0 && m == 100){
            return Aleatorio.ceroACien();  // Ya se había resuleto
        }else{
            double prob = 1.0/(m - n);  // dist. uniforme
            //System.out.println("Probabilidad: " + prob);
            double res = Math.random()/prob;  // Ver en que segmento cae
            res += n;  // Para empezar desde el intervalo dado
            //System.out.println(res);  // Impresion del valor en double
            //if de una línea que hace el redondeo
            return (int)res + 0.5 > (int) res? (int) res + 1: (int)res;
            /*Equivalente a:
            if((int)(res + 0.5) > (int)res){
                return (int) res + 1;
            }else{
                return (int)res;
            }
            */
        }
    }
    
    /**
     * Método que  calcula un valor aleatorio entre dos doubles dados
     * @param n double donde inicia el intervalo aleatorio
     * @param m double donde termina el intervalo aleatorio
     * @param decimales int número de decimales con los que se quiere la precisión
     * @return double entre n y m, distribución uniforme, -1 si n >= m o si n es negativo
     */
    public static double n_A_M(double n, double m, int decimales){
        if (n >= m || n < 0){
            return -1;  // Error
        }else if( decimales == 0){
            return Aleatorio.n_A_M((int) n, (int) m);  // Ya se había resuleto
        }else{
            double dec = Math.pow(10, decimales); //Valor para manejar los decimales
            double prob = 1.0/((m - n) * dec);  // Dist. Uniforme
            //System.out.println("Probabilidad: " + prob);
            double res = (1/dec) * (Math.random()/prob) + n;  // Calcula el valor aleatorio correspondiente desde n
            String num = Double.toString(res);  // String para acortar el resultado a los decimales pedidos
            return Double.parseDouble(num.substring(0, num.indexOf('.') + decimales + 1));
        }
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n, m, dec;
        double  n1, m1;
        dec = 5;
        n = 10;
        n1 = 0.5;
        m1 = 5.1;
        m = 4038;
        System.out.println("Valor aleatorio Entero entre 0 y 100 : " + Aleatorio.ceroACien());
        System.out.println("Valor aleatorio Entero entre "+ n +" y " + m + " : " + Aleatorio.n_A_M(n, m));
        System.out.println("Valor aleatorio Double entre "+ n1 +" y " + m1 + " : " + Aleatorio.n_A_M(n1, m1, dec));   
    }   
}