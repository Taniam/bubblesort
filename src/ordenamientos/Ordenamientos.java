package ordenamientos;
/**
 * @author Capeleishon, Estereishon y Tanieishon
 */
public class Ordenamientos {
    
    private int[] origArr;
    private int[] desArr;
    
    public Ordenamientos(int[] arr){
        origArr = arr;
    }
    
    public void setArreglo(int[] arr){
        desArr = arr;
    }
    /**
     * Intercambia el indice j con el k 
     * @param arr
     * @param j
     * @param k 
     */
    private static void intercambio(int[] arr,int j, int k ){
        int tmp = arr[j];
        arr[j] = arr[k];
        arr[k] = tmp;
    }
    
    public int[] BubbleSort(){
        int[] arrB = new int [origArr.length];
        //Copian el contenido del arreglo origonal a arrB
        for(int j = 0; j< arrB.length-1; j++){
            for(int k = j+1; k< arrB.length-1; k++){
                if(arrB[j] > arrB[k]){
                    intercambio(arrB, j, k);
                }
            }
        }
        return arrB;
    }    
}
